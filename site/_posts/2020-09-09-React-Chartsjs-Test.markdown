---
title: "React Chartjs 2 Test"
layout: post
date: 2020-09-09 19:04:06
tag:
- react
- javascript
- jekyll
category: blog
author: xulongli
description: React Chartjs 2 Test
---

# React + Jekyll

有了reactjs的支持，可以复用大量的react组件，例如绘图组件`echarts`, `chartjs`等等。

With the support of reactjs, we can reuse many react components in your blog, take chart drawing as an example here.

# Charts

如果React DOM成功，你的图会在这里哟！

If reactjs DOM build success, your charts will be here.

<div id="react-chartjs-2-demo"></div>
<script type="text/javascript" src="/assets/react/js/App.min.js"></script>


# Reference
[1] [jekyll-react-webpack](https://github.com/mlomboglia/jekyll-react-webpack.git)