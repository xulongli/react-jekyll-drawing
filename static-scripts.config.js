// Configures gulp build
// See gulpfile.babel.js for build pipeline
const { resolve } = require("path")

module.exports = function(env) {
  const dest = "site/"
  const build = "dist/"
  const isProduction = process.env.NODE_ENV === "production"

  return {
    generator: {
      label: "Jekyll",
      command: "bundle",
      args: {
        default: [
          "exec",
          "jekyll",
          "build",
          "--source",
          resolve(dest),
          "--destination",
          resolve(build),
        ],
        development: [
          "--drafts",
          "--unpublished",
          "--future",
          "--baseURL=/",
        ],
        preview: ["--baseURL=/"],
        production: [],
      },
    },
    styles: {
      dest: dest + "assets/react/css/",
    },
    scripts: {
      dest: dest + "assets/react/js/",
    },
    images: {
      dest: dest + "assets/react/img/",
    },
    svg: {
      dest: dest + "assets/react/svg/",
    },
  }
}
