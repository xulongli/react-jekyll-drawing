import React, { PureComponent } from 'react';

import Simple from './Simple.jsx';
import Events from './Events.jsx';
// import Theme from './Theme.jsx';
import Loading from './Loading.jsx';
import Api from './Api.jsx';
// import Dynamic from './Dynamic.jsx';
import Map from './Map.jsx';
import Gl from './Gl.jsx';

// v1.2.0 add 7 demo.
import Airport from './Airport.jsx';
import Calendar from './Calendar.jsx';
// import Gauge from './Gauge.jsx';
import Gcalendar from './Gcalendar.jsx';
import Graph from './Graph.jsx';
import Lunar from './Lunar.jsx';
import Treemap from './Treemap.jsx';
import Sunburst from './Sunburst.jsx';
import Svg from './Svg.jsx';

export default class myEChartApp extends  React.Component {
	render() {
		return (
			<div>
				<hr />
				<Simple />
				<hr />
				<Events />
				<hr />
				{/* <Theme /> */}
				<hr />
				<Loading />
				<hr />
				<Api />
				<hr />
				{/* <Dynamic /> */}
				<hr />
				<Map />
				<hr />
				<Gl />
				<hr />
				<Airport />
				<hr />
				<Calendar />				
				<hr />
				{/* <Gauge /> */}
				<hr />
				<Gcalendar />
				<hr />
				<Graph />
				<hr />
				<Lunar />
				<hr />
				<Treemap />
				<hr />
				<Sunburst />
				<hr />
				<Svg />
			</div>
		);
	}
}
