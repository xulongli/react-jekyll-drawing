import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Hello from './components/Hello';
import './App.scss';
import myimg from './assets/1.png';

// Viz
import ChartsApp from './components/chartjs/app';
import myEChartApp from './components/echarts/app';

import Simple from './components/echarts/Simple.jsx';
import Events from './components/echarts/Events.jsx';
// import Theme from './components/echarts/Theme.jsx';
import Loading from './components/echarts/Loading.jsx';
import Api from './components/echarts/Api.jsx';
// import Dynamic from './components/echarts/Dynamic.jsx';
import Map from './components/echarts/Map.jsx';
import Gl from './components/echarts/Gl.jsx';
// v1.2.0 add 7 demo.
import Airport from './components/echarts/Airport.jsx';
import Calendar from './components/echarts/Calendar.jsx';
// import Gauge from './components/echarts/Gauge.jsx';
import Gcalendar from './components/echarts/Gcalendar.jsx';
import Graph from './components/echarts/Graph.jsx';
import Lunar from './components/echarts/Lunar.jsx';
import Treemap from './components/echarts/Treemap.jsx';
import Sunburst from './components/echarts/Sunburst.jsx';
import Svg from './components/echarts/Svg.jsx';

const chartjsElement = document.getElementById('react-chartjs-2-demo');
if (chartjsElement) {
  ReactDOM.render(<ChartsApp />, chartjsElement); 
}

const ecartsElement = document.getElementById('react-echarts-demo');
if (ecartsElement) {
  // ReactDOM.render(myEChartApp, ecartsElement); 
  ReactDOM.render((<div><hr /><Simple /><hr /><Events /><hr /><Loading /><hr /><Api /><hr /><Map /><hr /><Gl /><hr /><Airport /><hr /><Calendar /><hr /><Gcalendar /><hr /><Graph /><hr /><Lunar /><hr /><Treemap /><hr /><Sunburst /><hr /><Svg /></div>), ecartsElement);
}

